package com.accenture.PruebaSerenityMaven.pasos;

import com.accenture.PruebaSerenityMaven.pages.GooglePage;

import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Step;

public class TesterSteps {
	
	GooglePage googlepage;
	
    @Step
    public void abreElNavegador() {
        googlepage.open();
    }
    
    @Step
    public void escribeEnElBuscador() {
    	googlepage.escribeEnLaBarraDeBusqueda();
    }
    
}
