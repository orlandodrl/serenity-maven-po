package com.accenture.PruebaSerenityMaven.features.busqueda;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import com.accenture.PruebaSerenityMaven.pasos.TesterSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;

@RunWith(SerenityRunner.class)
public class HolaMundo {
	
	@Managed(driver = "chrome", uniqueSession = true)
    public WebDriver driver;
	
	@Steps
    public TesterSteps tester;
	
	@Test
    public void busquedaDeHolaMundoEnGoogle() {	
		tester.abreElNavegador();
		tester.escribeEnElBuscador();
    }
	
}
